class Model:
    def __init__(self):                                                # Constructure for buid object
        self.table = [[' ',' ',' '],[' ',' ',' '],[' ',' ',' ']]       # List size 3x3 for storage X & O from input
    def get_table(self):
        return self.table
    
    def set_table(self,x,y,symbol):
        self.table[x][y] = symbol 
        
class Viewer:
    def __init__(self,Model):
        self.m = Model         
    def show_table(self):                                               # Draw table and show what is inside in each collum and row
        num = 1
        for i in range(0,len(self.m.get_table())):                
            for j in range(0,len(self.m.get_table()[i])):
                print("|"+self.m.get_table()[i][j] ,end= "|")
            print("   " ,end="")
            print("|"+str(num)+"|"+"|"+str(num+1)+"|"+"|"+str(num+2)+"|" ,end= "")
            num += 3
            print()
    def inputMessage(self,mark):
        print("\nThis is "+mark+" Turn")
        print("Select your move\n")

class Controller:
    
    def __init__(self,Model,Viewer):
        self.m = Model
        self.v = Viewer
        self.mark = ["X","O"]
        self.input 
    def add_mark(self,turnNumber):                            # Method that put X in table with specific loaction row and collum
        symbol = "X"
        markLocation = self.playerInput()
        print(self.playerInput())
        self.m.set_table(markLocation, markLocation,symbol)
        self.get_showTable()                        # Then after put O in table then show what it looks like
    def get_showTable(self):
        self.v.show_table()
    def get_inputMessage(self,turnNumber):
        self.v.inputMessage(self.mark[turnNumber%2])
        self.playerInput()
    def playerInput(self):
        receiveMark = int(input(" "))
        return receiveMark    
    def checkerror(self,n):                        # Check if input are only 0, 1, 2 if not then re-input
        if(len(n) != 1 or ord(n) < 49 or ord(n) > 57 ):
            print("\nERROR!!! : Try Again\n")
            self.get_showTable()
            return True
        
        x = float(n)/3
        y = float(n)%3
        x= int(x)
        y = int(y)
        if(self.table[x][y] != " " ):
            print("\nERROR!!! : Try Again\n")
            self.get_showTable()
            return True
        else:
            return False
def main():                                         # Main function to run Tic Tac Toe
    storage_data = Model()
    display = Viewer(storage_data)
    control = Controller(storage_data,display)
    count_loop = 1
    while(True):
        control.get_showTable()
        control.get_inputMessage(count_loop)
        control.add_mark(count_loop)
        count_loop += 1
    
main()